# ProcessQueue

Class with the function of simply managing a process queue using the lliure-core Model


## How to create the model?

``` php
namespace \Models;

use ProcessQueue\ProcessQueueModel;

class Queue extends ProcessQueueModel
{
    protected static array $fields = ['id', 'attempts']; // define your fields, is required to have id and attempts fields
    protected static ?string $table = 'queue'; // define your table
    protected static $attempts = 5; // opcional, set maximum attempts 
}
```

## How to use?

``` php
$processes = Queue::getProcess(); //capturing available processes

foreach ($processes as $process){
    $process->start(); // starting the process

    if($myteste){
        $process->confirmProcess(); //confirming a process
    } else {
        $process->errorProcess('Erro of process'); //reporting an error
    }
}
```