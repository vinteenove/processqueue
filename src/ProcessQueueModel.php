<?php

namespace ProcessQueue;

use LliureCore\Collection;
use LliureCore\Model;

abstract class ProcessQueueModel extends Model implements ProcessQueueInterface
{
    protected static array $fields = ['id', 'attempts'];

    protected static $attempts = 5;

    /**
     * @return string
     */
    public static function getFields() : string
    {
        if (empty(static::$fields)) {
           return '*';
        }

        return '`'.join('`, `', static::$fields).'`';
    }

    /**
     * @param int $limit
     * @return Collection
     * @throws \Exception
     */
    public static function getProcess(int $limit = 10) : \LliureCore\Collection
    {
        return static::findMany('SELECT ' . static::getFields()
                                . ' FROM ' . static::getTable()
                                . ' WHERE (status =%s OR status =%s) AND attempts < %i ORDER BY id ASC LIMIT %i',
                                ProcessQueueInterface::STATUS_PENDING,
                                ProcessQueueInterface::STATUS_ERROR,
                                static::$attempts,
                                $limit);
    }

    /**
     * @return null
     * @throws \Exception
     */
    public function start()
    {
        if(empty($this->data)){
            return null;
        }

        $this['attempts'] += 1;
        $this['status'] = ProcessQueueInterface::STATUS_PROCESSING;

        $this->update();
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function confirmProcess(){
        $this->delete();
        return true;
    }

    /**
     * @param string $error
     * @return bool
     * @throws \Exception
     */
    public function errorProcess(string $error){
        $this['lastError'] = $error;
        $this['status'] = ProcessQueueInterface::STATUS_ERROR;
        $this->update();

        return true;
    }

}