<?php
namespace ProcessQueue;

use ProcessQueue\Models\Queue as QueueModel;

class ProcessQueue
{
    private $data;

    /**
     * @param $service
     * @param null $data
     * @throws \Exception
     */
    static function addProcess($service, $data = null){
        $service = ['service' => $service, 'data' => $data];

        QueueModel::build($service)->insert();
    }

    /**
     * @return QueueModel|null
     * @throws \Exception
     */
    function getProcess()
    {
        $processes =  QueueModel::getProcess();

        foreach ($processes as $process){
            $process->start();

            $this->data = $process;

            return $process;
        }

        return null;
    }

    /**
     * @return bool
     */
    function confirmProcess(){
        return $this->data->confirmProcess();
    }

    /**
     * @param $error
     * @return bool
     */
    function errorProcess($error){
        return $this->data->errorProcess($error);
    }
}