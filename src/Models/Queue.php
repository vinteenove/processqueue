<?php

namespace ProcessQueue\Models;

use ProcessQueue\ProcessQueueModel;

class Queue extends  ProcessQueueModel
{

    protected static $primaryKey = 'id';
    protected static ?string $table = 'processQueue_queue';

    protected static array $fields = ['id', 'attempts', 'service', 'data'];

}