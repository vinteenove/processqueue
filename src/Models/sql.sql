CREATE TABLE `ll_processQueue_queue` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`status` ENUM('0','1','2','3') NOT NULL DEFAULT '0' COMMENT '\'0\': pendente,\'1\': processando ,\'2\': processado,\'3\': erro',
	`service` VARCHAR(100) NOT NULL,
	`data` TEXT NOT NULL,
	`attempts` INT(11) NOT NULL DEFAULT '0',
	`lastError` TEXT NULL,
	`insertedAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`processedAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;
