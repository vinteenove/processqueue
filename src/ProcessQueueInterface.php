<?php

namespace ProcessQueue;

interface ProcessQueueInterface
{
    const STATUS_PENDING = '0';
    const STATUS_PROCESSING = '1';
    const STATUS_FINISH = '2';
    const STATUS_ERROR = '3';

    public static function getFields() : string;

    public static function getProcess(int $limit = 10) : \LliureCore\Collection;

    public function start();

    public function confirmProcess();

    public function errorProcess(string $error);
}